allowed_ips = "Allowed IP List"

waf_allowed = "ACL of allowed IPs"

blocked_ipset = "Blocked IP List"

waf_blocked = "ACL of blocked IPs"

xss = "Xss Rules"

waf_xss = "List of Xss rules allowed on the ACL"

sql = "SQL Injection Rules"

waf_sql = "List of rules for SQL Injection on the ACL"

byte = "Byte Match rules"

waf_byte = "List of Byte Match Rules on the ACL"

size = "Size Restriction rules"

waf_size = "List of Size Restriction rules on the ACL"

allowedips = [
  {
    value = "38.142.251.10/32"
    type  = "IPV4"
  },
  {
    value = "72.54.9.134/32"
    type  = "IPV4"
  }
]

blockedips = [
  {
    value = "173.46.141.176/32"
    type  = "IPV4"
  },
  {
    value = "35.194.60.255/32"
    type  = "IPV4"
  }
]

xssrule = [
  {
    text_transformation = "URL_DECODE"
  },
  {
    text_transformation = "HTML_ENTITY_DECODE"
  }
]

sqlrule = [
  {
    text_transformation = "URL_DECODE"
  },
  {
    text_transformation = "HTML_ENTITY_DECODE"
  }
]

sizerule = [
  {
    text_transformation = "NONE"
    comparison_operator = "GT"
    size                = "8192"
    type                = "BODY"
  }
]

byterule = [
  {
    text_transformation   = "NONE"
    target_string         = "V2CgwWyRS"
    positional_constraint = "EXACTLY"
    type                  = "HEADER"
    data                  = "x-api-key"
  }
]
