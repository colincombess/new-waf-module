variable "env" {}

variable "stage" {}

variable "allowed_ips" {
}

variable "waf_allowed" {
}

variable "blocked_ipset" {
}

variable "waf_blocked" {
}

variable "xss" {
}

variable "waf_xss" {
}

variable "sql" {
}

variable "waf_sql" {
}

variable "byte" {
}

variable "waf_byte" {
}

variable "size" {
}

variable "waf_size" {
}

variable "allowedips" {
  type = list(object({
    value = string
    type  = string
  }))

  description = "List of one or more custom error response element maps"
  default     = []
}

variable "blockedips" {
  type = list(object({
    value = string
    type  = string
  }))

  description = "List of one or more custom error response element maps"
  default     = []
}

variable "xssrule" {
  type = list(object({
    text_transformation = string
  }))

  description = "List of one or more custom error response element maps"
  default     = []
}

variable "sqlrule" {
  type = list(object({
    text_transformation = string
  }))

  description = "List of one or more custom error response element maps"
  default     = []
}

variable "sizerule" {
  type = list(object({
    text_transformation = string
    comparison_operator = string
    size                = string
    type                = string
  }))

  description = "List of one or more custom error response element maps"
  default     = []
}

variable "byterule" {
  type = list(object({
    text_transformation   = string
    target_string         = string
    positional_constraint = string
    type                  = string
    data                  = string
  }))

  description = "List of one or more custom error response element maps"
  default     = []
}
