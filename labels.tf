module "waf_labels" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=0.15.0"
  namespace   = "pm"
  environment = var.env
  stage       = var.stage
  delimiter   = "-"

  tags = {
    "Bill To"    = "test"
    "Managed By" = "test"
    Product      = "Test"
    Component    = "Test"
  }
}
