module "waf_test_label" {
  source  = "git::https://github.com/cloudposse/terraform-null-label.git?ref=0.15.0"
  context = module.waf_labels.context
  name    = "Waf test"
}

module "waf_test" {
  source = "git::ssh://git@bitbucket.org/parkmobileus/terraform-aws-waf.git"

  name_allowed_ipset = var.allowed_ips
  name_waf_allowed   = var.waf_allowed
  name_blocked_ipset = var.blocked_ipset
  name_waf_blocked   = var.waf_blocked
  name_xss           = var.xss
  name_waf_xss       = var.waf_xss
  name_sql           = var.sql
  name_waf_sql       = var.waf_sql
  name_byte          = var.byte
  name_waf_byte      = var.waf_byte
  name_size          = var.size
  name_waf_size      = var.waf_size

  allowedips = var.allowedips
  blockedips = var.blockedips
  xssrule    = var.xssrule
  sqlrule    = var.sqlrule
  sizerule   = var.sizerule
  byterule   = var.byterule
}